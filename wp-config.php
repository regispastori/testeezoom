<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'wordpress' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ijZX32} DKlJh{rH<W(:!r.uVu4?hJ^0_i4#;[>x#>-Jf.h~`w|_4c{ic4J$fMKw' );
define( 'SECURE_AUTH_KEY',  'I3c1F]l5V$ZNgda;@gzz:7.uYC$}FA>jyHPkI62b;XpwS%y*dyu+&O[Q`@)=4sc]' );
define( 'LOGGED_IN_KEY',    '^wr7T7F!@GFv~u`;PHF)aM6wKpMds%x{~`}>U@Mn<iH*o&E%qK3 ESz4!:[T3g0z' );
define( 'NONCE_KEY',        'l&Brx X,mbb>~Ia-b(==yIgHs3({/S(uT{@~_B_DUT}6TM6~Zl!P,dmt3uU1FUzG' );
define( 'AUTH_SALT',        'Ut7gtM}X5Wdg^MVe]&CCr@!_kt?9Vv*/X_>glO ?$uWPGV9TWdU:t3s6^X>h0mV%' );
define( 'SECURE_AUTH_SALT', '</syO3AuD{QlVw_U*^{o/pF.FlSZ(od-I_]$&@@C}h`jtvP&Ig$I!;[PxKc/&&Z8' );
define( 'LOGGED_IN_SALT',   '&gFZ5:k8b&QbkH4EdW~7+swi+Ji_rW$,+@Ev09$XT7IrTj(9CbGfj !j8 &W@3<M' );
define( 'NONCE_SALT',       'ym1yI/dD8-2 >Nkaszia.k>:;$bMv De_b^][wK=eJ)@</Pb)!&yNr!K4G&Ke&8v' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
